# TP3.sql
#utilisateur ROOT

DROP DATABASE IF EXISTS teams;
CREATE DATABASE teams;

USE teams;

DROP TABLE IF EXISTS games;

CREATE TABLE games (
    match_date DATETIME,
    observations VARCHAR(255),
    victory INT
);

DROP TABLE IF EXISTS players;

CREATE TABLE players (
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    start_date DATE
);

DROP USER IF EXISTS manager;
CREATE USER 'manager'@'%' IDENTIFIED BY 'manager_password';
GRANT ALL PRIVILEGES ON teams.games TO 'manager'@'%';

DROP USER IF EXISTS recruiter;
CREATE USER 'recruiter'@'%' IDENTIFIED BY 'recruiter_password';
GRANT INSERT,SELECT ON teams.players TO 'recruiter'@'%';

FLUSH PRIVILEGES;
