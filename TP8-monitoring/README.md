# TP 8 - Monitoring with prometheus

## Intro

Prometheus is a systems and service monitoring system. It collects metrics from configured targets at given intervals, evaluates rule expressions, displays the results, and can trigger alerts if some condition is observed to be true.

## Setup

### **Services**
 
* Mysql container
* Mysql exporter container
* prometheus container

Mysql container is the database we want to check \
Mysql exporter is requiered to translate de metrics for prometheus \
prometheus is the monitor

### ***setup***

For this TP we have to setup 3 kind of things :
- The data base (mysql):
    ``` db:
    image: mariadb
    restart: on-failure
    environment:
      - MYSQL_ROOT_PASSWORD=password
    volumes:
      - ./sql/:/docker-entrypoint-initdb.d/:ro
    networks:
      - bdd
    expose:
      - 3306
    ports: 
      - "3306:3306"

- The exporter to communicate data from mysql to prometheus: 

    ``` exporter:
    image: prom/mysqld-exporter
    container_name: mysql-exporter
    environment:
      - DATA_SOURCE_NAME=exporter:password@(db:3306)/
    ports:
      - "9104:9104"
    expose:
      - 9104
    networks:
      - bdd
      - exporter
    depends_on:
      - db
- Prometheus to manage data and alerts from the exporter:

    ``` prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    ports:
    - 9090:9090
    networks:
      - bdd
      - exporter
    volumes:
    - ./config/prometheus.yml:/etc/prometheus/prometheus.yml:ro

### ***exploitation***

Go to localhost:9090/graph on your favorite navigator and choose this filter : mysql_global_status_commands_total{command="delete"} \
with this filter you can visualize all READ and WRITE opérations in your mysql database