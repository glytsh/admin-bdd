DROP DATABASE IF EXISTS TP6_AT;
CREATE DATABASE TP6_AT;
USE TP6_AT;

DROP TABLE IF EXISTS clients;

CREATE TABLE clients (
    nom VARCHAR(255),
    prenom VARCHAR(255),
    naissance DATE,
    cp INT(5)
);

INSERT INTO clients VALUES ("Lucas", "Requena", "1998-02-11", "33200"), ("Bob", "TRUC", "2000-04-14", "33000"), ("Eve", "MACHIN", "1998-06-24", "33000");

SELECT * FROM clients;