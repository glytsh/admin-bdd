CREATE USER 'replicant'@'%' IDENTIFIED BY 'password';

GRANT replication slave ON '*' TO 'replicant'@'%';

FLUSH PRIVILEGES;